//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _LED7SEG_H
#define _LED7SEG_H

#include "..\types.h"


extern uint8_t led7seg_buff[];


// ��������� �������������� �������
void led7seg_init(void);
// ��������� ��������� ������� Test �������
void led7seg_testmatrix(uint8_t TestOn);
// ��������� ������������� ������� �������. �������� 0-15
void led7seg_set_brightness(uint8_t Value);
// ��������� ��������� ����� ����� ��������� FillValue
void led7seg_fill_screenbuff(uint8_t FillValue);
// ��������� ������ ������ �� ������ Str �� ��������� (��� ������������� ���������������� ������)
void led7seg_DrawString(int16_t X, uint8_t *Str);
// ��������� ������� �� ��������� ��������������� ������
void led7seg_printf(int16_t X, const char *args, ...);
// ��������� ��������� ��������� ����������� � ������������ � ������� ������ ledmatrix_screenbuff
void led7seg_update_from_buff(void);

#endif