#include <stm8s.h>
#include <led7seg.h>
#include <delay.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>


uint16_t Cnt = 0;

int main( void )
{
  CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV1);      // 16 / 1 ���
  
  // �������������� ������������ ���������
  led7seg_init();
  
  // ������ ����� ��������� (���� ���� �����������)
  led7seg_testmatrix(1);
  delay_ms(500);
  led7seg_testmatrix(0);

  while (1)
  {
    //printf("%d.%d\n", Cnt / 10, Cnt % 10);
    led7seg_printf(0, "%d.%d", Cnt / 10, Cnt % 10);
    Cnt++;
      
    led7seg_update_from_buff();
    
    delay_ms(100);
  }
}
